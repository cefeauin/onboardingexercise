import axios from "axios";
import { ref } from "vue";

export default {
  name: "DashboardPage",

  data() {
    return {
      link: ref("menu3"),
      cardList: [
        {
          title: "CARD 1",
          description: "Description",
          route_name: "table-page",
        },
        {
          title: "CARD 2",
          description: "Description",
          route_name: "table-page",
        },
        {
          title: "CARD 3",
          description: "Description",
          route_name: "table-page",
        },
        {
          title: "CARD 4",
          description: "Description",
          route_name: "table-page",
        },
      ],
    };
  },

  methods: {
    navigateToTablePage(card) {
      const routeName = card.route_name;

      this.$router.push({ name: routeName });
    },
  },
};
